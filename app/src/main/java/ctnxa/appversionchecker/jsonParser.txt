{
  "android": {
    "releaseNotes": [
      {
        "start": "What&#39;s new"
      },
      {
        "parseInsideTag": "description"
      }
    ],
    "updatedDate": [
      {
        "start": "Updated on"
      },
      {
        "parseInsideTag": "xg1aie"
      }
    ],
    "version": [
      {
        "start": "You can request that data be deleted",
        "startConstant": 36
      },
      {
        "start": "You can request that data be deleted",
        "startConstant": 36
      },
      {
        "start": "[[[",
        "startConstant": 4
      },
      {
        "end": "->releaseNotes"
      },
      {
        "end": "\"]]"
      },
      {
        "end": "\"]]"
      }
    ]
  }
}