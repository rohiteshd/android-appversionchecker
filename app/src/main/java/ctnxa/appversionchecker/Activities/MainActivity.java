package ctnxa.appversionchecker.Activities;

import android.content.BroadcastReceiver;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import ctnxa.appversionchecker.Utils.AppVersionCheckerInit;

public class MainActivity extends AppCompatActivity {
    BroadcastReceiver broadcastReceiver;

    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
    }

    @Override
    public void onResume(){
        super.onResume();

        // sample initialization
        new AppVersionCheckerInit(true, true, 0, "ctnxa.cityConn", this);
    }
}