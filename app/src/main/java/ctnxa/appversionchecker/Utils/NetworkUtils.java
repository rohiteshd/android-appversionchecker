package ctnxa.appversionchecker.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import okhttp3.ConnectionPool;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NetworkUtils {
    Context context;
    String response;
    boolean isGetOrPost;
    String urlStr;
    boolean isResponseReceived;

    // okhttp related
    RequestBody formBody;
    private static ConnectionPool connectionPool = new ConnectionPool(1, 30, TimeUnit.SECONDS);
    private static OkHttpClient httpClient = null;
    Request requestObj;

    // check internet connected
    boolean isInternet;
    static int loopsThreshold = 3;
    static long waitThreshold = 3000;

    // constructor
    public NetworkUtils(Context context, HashMap<String, String> postParameters, boolean isGetOrPost, String urlStr){
        String key, value;
        int count = 0;
        Iterator<String> itr;

        // fprm body related
        FormBody.Builder formBodyBuilder;

        // initializations
        this.context = context; this.isGetOrPost = isGetOrPost; this.urlStr = urlStr; this.response = ""; this.isResponseReceived = false; this.isInternet = false;

        // append params to url if get
        if(this.isGetOrPost && postParameters!=null){
            itr = postParameters.keySet().iterator();
            while(itr.hasNext()){
                key = itr.next(); value = postParameters.get(key);

                // prepare the url if get request
                this.urlStr += count==0 ? "?" : "";
                this.urlStr += count>0 ? "&" : "";
                this.urlStr += key + "=" + value;
                count++;
            }
        }

        // create form body for post
        if(!this.isGetOrPost && postParameters!=null){
            itr = postParameters.keySet().iterator(); formBodyBuilder = new FormBody.Builder();
            while(itr.hasNext()){
                key = itr.next(); value = postParameters.get(key);

                // add to form body
                formBodyBuilder.add(key, value);
            }

            // create form body
            formBody = formBodyBuilder.build();
        }

        // randomize the URL
        this.urlStr += this.urlStr.contains("?") ? "&" : "?";
        this.urlStr += "randomize=" + Calendar.getInstance().getTimeInMillis();

        // form request object
        requestObj = isGetOrPost ?
                new Request.Builder().url(urlStr).build() :
                new Request.Builder().url(urlStr).post(formBody).build();

        // initialize client if not done (this is singleton)
        if(httpClient == null)
            httpClient = new OkHttpClient.Builder()
                    .connectionPool(connectionPool)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .build();
    }

    // check for network connectivity
    public static boolean isConnectedToInternet(Context context){
        NetworkInfo networkInfo;
        boolean isConnected;

        // get status
        networkInfo = ((ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        isConnected = (networkInfo!=null && networkInfo.isAvailable() && networkInfo.isConnectedOrConnecting());

        return isConnected;
    }

    // get the response
    public String getResponse(){
        int count = 0;
        int sendingResponseCount = 0;

        // okhttp related
        Response responseObj;

        // repeat till update sent to server
        do {
            // wait till internet is available
            while(!isInternet && count++<loopsThreshold){
                isInternet = NetworkUtils.isConnectedToInternet(context);
                if(!isInternet) {
                    try {
                        Thread.sleep(waitThreshold);
                    } catch (InterruptedException e) {}
                }
            }

            // proceed if internet is available
            if(isInternet){
                try {
                    // get the response
                    responseObj = httpClient.newCall(requestObj).execute();
                    if(responseObj.isSuccessful())
                        response = responseObj.body().string();

                    // get and trim response and update flag
                    response = response!=null ? response.trim() : response;
                    isResponseReceived = true;
                } catch(IOException e) {}
            }
        } while(isInternet && !isResponseReceived && sendingResponseCount++<loopsThreshold);

        return response;
    }
}