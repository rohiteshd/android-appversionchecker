package ctnxa.appversionchecker.Utils;

import android.content.Context;

import java.util.Calendar;
import java.util.TimeZone;

public class TimeUtils {
    public static final int TYPE_DATEONLY = 1;
    public static final int TYPE_TIMEONLY = 2;
    public static final int TYPE_DATETIMEBOTH = 3;

	// get calendar
	public static Calendar getCalendar(boolean isLocalOrUTC){
		return isLocalOrUTC ? Calendar.getInstance() : Calendar.getInstance(TimeZone.getTimeZone("UTC"));
	}

	// get time stamp
	public static String getTimestamp(Calendar calendar, boolean isSecond, boolean isLocalOrUTC){
		String timeStamp_str = getDate_str(calendar, isLocalOrUTC) + getTime_str(isSecond, calendar, isLocalOrUTC);
		
		return timeStamp_str;
	}

	// convert to timezone
	public static String convertToTimezone(String timestamp, int offset, boolean toUTCOrFromUTC){
		Calendar calendar = getCalendar(true);
		String convertedTimestamp = "";
		boolean isSecond;

		// proceed if timestamp valid
		if(timestamp!=null && timestamp.length()>0){
			// check if second is required
			isSecond = timestamp.length() > 12;

			// set the calendar
			calendar.set(Calendar.YEAR, Integer.parseInt(timestamp.substring(0, 4)));
			calendar.set(Calendar.MONTH, Integer.parseInt(timestamp.substring(4, 6))-1);
			calendar.set(Calendar.DATE, Integer.parseInt(timestamp.substring(6, 8)));
			calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timestamp.substring(8, 10)));
			calendar.set(Calendar.MINUTE, Integer.parseInt(timestamp.substring(10, 12)));
			if(isSecond)
				calendar.set(Calendar.SECOND, Integer.parseInt(timestamp.substring(12, 14)));

			// convert
			offset *= toUTCOrFromUTC ? -1 : 1;
			calendar.add(Calendar.SECOND, offset);
			convertedTimestamp = getTimestamp(calendar, isSecond, false);

			// remove seconds if not in original
			if(!isSecond)
				convertedTimestamp = convertedTimestamp.substring(0, 12);
		}

		return convertedTimestamp;
	}

	// get time stamp in words
	public static String getTimestampWord(String timestamp, int type, Context context){
        String timestamp_word = "";
        String date_word = null;
        String time_word = null;

		try {
			// if timestamp == "0" ignore
			if(timestamp!=null && timestamp.length()>0 && timestamp.compareTo("0")!=0){
                switch(type){
                    case TYPE_DATEONLY:
                        // get date word and remove the year from it
                        date_word = getDateWord(timestamp.substring(0, 8));
                        date_word = date_word.substring(0, date_word.indexOf(", "));
                        timestamp_word = date_word;
                        break;

                    case TYPE_TIMEONLY:
						// append 0's to time if less than 4 digit in length
						while(timestamp.length() < 4)
							timestamp = "0" + timestamp;

                        // get time word and append to timestamp word
                        time_word = getTimeWord(timestamp.substring(0, 4));
                        timestamp_word = time_word;
                        break;

                    case TYPE_DATETIMEBOTH:
                        // get date word and remove the year from it
                        date_word = getDateWord(timestamp.substring(0, 8));
                        date_word = date_word.substring(0, date_word.indexOf(", "));
                        timestamp_word = date_word;

                        // process time if timestamp has the time component
                        if(timestamp.length() > 8) {
                            // get time word and append to timestamp word
                            time_word = getTimeWord(timestamp.substring(8, 12));
                            timestamp_word = time_word + ", " + date_word;
                        }
                        break;
                }
			}
		} catch(NumberFormatException e){
			// ErrorService.reportError("NumberFormatException", e.getStackTrace(), "TimeUtils :: getTimestampWord()", context);
		}

        return timestamp_word;
    }
	
	// get time as string
	public static String getTime_str(boolean isSecond, Calendar calendar, boolean isLocalOrUTC){
		String time_str = "";

		// instantiate calendar if not already done
		if(calendar == null)
			calendar = getCalendar(isLocalOrUTC);
		
		// create the string and convert to integer
		if(calendar.get(Calendar.HOUR_OF_DAY) < 10)
			time_str += "0";
		time_str += Integer.toString(calendar.get(Calendar.HOUR_OF_DAY));
		if(calendar.get(Calendar.MINUTE) < 10)
			time_str += "0";
		time_str += Integer.toString(calendar.get(Calendar.MINUTE));

		// add seconds if requested
		if(isSecond){
			if(calendar.get(Calendar.SECOND) < 10)
				time_str += "0";
			time_str += Integer.toString(calendar.get(Calendar.SECOND));
		}
		
		return time_str;
	}
	
	// get date as string
	public static String getDate_str(Calendar calendar, boolean isLocalOrUTC){
		String date_str;

		// instantiate calendar if not already done
		if(calendar == null)
			calendar = getCalendar(isLocalOrUTC);
		
		// create the string and convert to integer
		date_str = Integer.toString(calendar.get(Calendar.YEAR));
		if((calendar.get(Calendar.MONTH)+1) < 10)
			date_str += "0";
		date_str += Integer.toString(calendar.get(Calendar.MONTH)+1);
		if(calendar.get(Calendar.DATE) < 10)
			date_str += "0";
		date_str += Integer.toString(calendar.get(Calendar.DATE));
		
		return date_str;
	}

	// get calendar from timestamp
	public static Calendar getCalendar(String initTimestamp_str, int type, int count, boolean isLocalOrUTC){
        Calendar calendar = getCalendar(isLocalOrUTC);

        // set the calendar to the specified date
		if(initTimestamp_str!=null && initTimestamp_str.length()>0){
			calendar.set(Calendar.YEAR, Integer.parseInt(initTimestamp_str.substring(0, 4)));
			calendar.set(Calendar.MONTH, Integer.parseInt(initTimestamp_str.substring(4, 6))-1);
			calendar.set(Calendar.DATE, Integer.parseInt(initTimestamp_str.substring(6, 8)));

			// set the calendar to the specified time
			if(initTimestamp_str.length() > 8){
				calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(initTimestamp_str.substring(8, 10)));
				calendar.set(Calendar.MINUTE, Integer.parseInt(initTimestamp_str.substring(10, 12)));

				// set seconds
				if(initTimestamp_str.length() > 12)
					calendar.set(Calendar.SECOND, Integer.parseInt(initTimestamp_str.substring(12, 14)));
			}
		}

        // add the count
        calendar.add(type, count);

        return calendar;
    }
	
	// get the word time string
	private static String getTimeWord(String time_str){
		String timeWord = "";
		int hours;
		String amPM;

        // ensure time has 4 digits
        for(int x=0; time_str.length()<4 && x<(4-time_str.length()); x++)
            time_str = "0" + time_str;

		// determine AM/PM and reduce hours by 12 if PM
		hours = Integer.parseInt(time_str.substring(0, time_str.length()-2));
		amPM = hours < 12 ? "AM" : "PM";
		hours -= hours<=12 ? 0 : 12;
		
		// insert hours and minutes separator
		timeWord = hours + ":" + time_str.substring(time_str.length()-2, time_str.length()) + amPM;
		
		return timeWord;
	}
	
	// get the word date string
	private static String getDateWord(String fullDate_str){
		String dateWord = "";
		int date;
		int month;
		
		// get the date word string
		date = Integer.parseInt(fullDate_str.substring(6, 8));
		dateWord += date;
		switch(date){
			case 1:
			case 21:
			case 31:
				dateWord += "st";
				break;
				
			case 2:
			case 22:
				dateWord += "nd";
				break;
				
			case 3:
			case 23:
				dateWord += "rd";
				break;
				
			default:
				dateWord += "th";
				break;
		}
		
		// get the month word string
		month = Integer.parseInt(fullDate_str.substring(4, 6));
		switch(month){
			case 1:
				dateWord += " January";
				break;

			case 2:
				dateWord += " February";
				break;

			case 3:
				dateWord += " March";
				break;

			case 4:
				dateWord += " April";
				break;

			case 5:
				dateWord += " May";
				break;

			case 6:
				dateWord += " June";
				break;

			case 7:
				dateWord += " July";
				break;

			case 8:
				dateWord += " August";
				break;

			case 9:
				dateWord += " September";
				break;

			case 10:
				dateWord += " October";
				break;

			case 11:
				dateWord += " November";
				break;

			case 12:
				dateWord += " December";
				break;
		}
		
		// year
		dateWord += ", " + fullDate_str.substring(0, 4);
		
		return dateWord;
	}

	// get calendar from date and time int's
    public static Calendar getCalendar(int date_int, int timeWithoutSeconds_int, boolean isLocalOrUTC){
        String date_str = Integer.toString(date_int);
        String timeWithoutSeconds_str = Integer.toString(timeWithoutSeconds_int);
        Calendar calendar = getCalendar(isLocalOrUTC);

        // set the date
        calendar.set(Calendar.YEAR, Integer.parseInt(date_str.substring(0, 4)));
        calendar.set(Calendar.MONTH, Integer.parseInt(date_str.substring(4, 6))-1);
        calendar.set(Calendar.DATE, Integer.parseInt(date_str.substring(6, 8)));

        // set the time
        if(timeWithoutSeconds_int > 0){
            timeWithoutSeconds_str = timeWithoutSeconds_str.length()<4 ? "0"+timeWithoutSeconds_str : timeWithoutSeconds_str;
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(timeWithoutSeconds_str.substring(0, 2)));
            calendar.set(Calendar.MINUTE, Integer.parseInt(timeWithoutSeconds_str.substring(2, 4)));
        }

        return calendar;
    }
}