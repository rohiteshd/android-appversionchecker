package ctnxa.appversionchecker.Utils;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

public class Parser {
    Context context;

    // parer JSON
    JSONObject parserJSON = null;

    // os name
    static final String OSNAME = "android";

    // parser keys
    static final String KEY_PARSEINSIDEELEMENT = "parseInsideTag";
    static final String KEY_START = "start";
    static final String KEY_STARTCONSTANT = "startConstant";
    static final String KEY_END = "end";
    static final String KEY_ENDCONSTANT = "endConstant";

    // special char for extra param
    static final String SPECIALCHAR_EXTRA_PARAM = "->";

    // constructor
    public Parser(Context context){
        this.context = context;

        try {
            // get the parser
            this.parserJSON = new JSONObject(new NetworkUtils(context, null, true, Urls.parser).getResponse());
        } catch(JSONException e){
            e.printStackTrace();
            this.parserJSON = null;
        }
    }

    // get the content inside an element
    private String parseInsideElement(String searchStr, String response){
        String content = null, tag;
        int maxCount = 10, count = 0;
        int start, end;
        boolean isValidTag;

        do {
            // check if search string lies inside element
            end = response.indexOf(searchStr); end = response.indexOf(">", end);
            start = response.lastIndexOf("<", end)+1;
            tag = response.substring(start, end);

            // for a tag to be valid, there should be no other child tag inside and tag shoudn't end without any content
            isValidTag = tag.indexOf("<")<0 && tag.indexOf(">")<0 && tag.charAt(tag.length()-2)!='/';

            // get the value inside the element
            if(isValidTag) {
                start = end+1; end = response.indexOf("</", start);
                content = response.substring(start, end);
            }

        } while(count<maxCount && content==null);

        return content;
    }

    // parse a value from network response
    public String parseValueFromHTML(String response, String jsonKey, HashMap<String, String> extraParams){
        // parse related
        boolean isDone = false, isResponseValid = StringUtils.isValid(response);
        int start, end, value_int;
        String key, value, value_final = null;

        // json related
        JSONObject json, jsonMain;
        JSONArray jsonArr;
        Iterator<String> itr;

        try {
            // parse through steps to get version
            jsonArr = parserJSON.getJSONObject(OSNAME).getJSONArray(jsonKey);
            for(int x=0; x<jsonArr.length() && isResponseValid; x++){
                json = jsonArr.getJSONObject(x);

                // parse through the keys
                itr = json.keys(); isDone = false; start = 0; end = response.length();
                while(itr.hasNext()){
                    key = itr.next(); value = null;

                    // key's value can be raw string or needs to be taken from extra params if beginning with special char
                    if(json.get(key) instanceof String && json.getString(key).indexOf(SPECIALCHAR_EXTRA_PARAM)==0) {
                        // remove the special char from the key's value and get the actual value from extra params
                        value = json.getString(key).substring(SPECIALCHAR_EXTRA_PARAM.length());
                        value = extraParams.containsKey(value) ? extraParams.get(value) : null;
                    }

                    // check what action to perform
                    switch(key){
                        case KEY_PARSEINSIDEELEMENT:
                            value = !StringUtils.isValid(value) ? json.getString(key) : value;
                            value_final = parseInsideElement(value, response); response = value_final;
                            isDone = true;
                            break;

                        case KEY_START:
                            // get start value from substring
                            value = !StringUtils.isValid(value) ? json.getString(key) : value;
                            start = response.indexOf(value, start);
                            break;

                        case KEY_STARTCONSTANT:
                            // add constant to start
                            value_int = json.getInt(key);
                            start += value_int;
                            break;

                        case KEY_END:
                            // get end value from substring
                            value = !StringUtils.isValid(value) ? json.getString(key) : value;
                            end = response.lastIndexOf(value, end);
                            break;

                        case KEY_ENDCONSTANT:
                            // add constant to end
                            value_int = json.getInt(key);
                            end += value_int;
                            break;
                    }
                }

                // update response and value
                if(!isDone) {
                    value_final = response.substring(start, end); response = value_final;
                }
            }
        } catch(JSONException e){
            e.printStackTrace();

            // set final value to null
            value_final = null;
        } catch(Exception e){
            e.printStackTrace();

            // set final value to null
            value_final = null;
        }

        return value_final;
    }
}