package ctnxa.appversionchecker.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class HelperUtils {
	public static final String TAG = "ctnxa.appVersionChecker";

    // log
    public static void log(boolean isLog, String message){
        // log ONLY if flag set
        if(isLog)
            Log.d(TAG, message);
    }

    // set shared preference value
    public static void setSharedPreferencesString(String sfFileName, String key, String value, Context context) {
        SharedPreferences settings = context.getSharedPreferences(sfFileName, 0);
        SharedPreferences.Editor settingsEditor;
        settingsEditor = settings.edit();
        settingsEditor.putString(key, value);
        settingsEditor.commit();
    }

    // get shared preference value
	public static String getSharedPreferencesString(String sfFileName, String sharedPref_key, Context context) {
		String sharedPref_value;

		// get the shared reference value
		sharedPref_value = context.getSharedPreferences(sfFileName, 0).getString(sharedPref_key, null);

		// special cases for null values
		if(sharedPref_value == null)
            switch(sharedPref_key){
                case Constants.VERSION_USERIGNOREDVERSION:
                    sharedPref_value = "";
                    break;
            }

		return sharedPref_value;
	}
}