package ctnxa.appversionchecker.Utils;

public class Constants {
    // shared preferences
    public static final String SHARED_PREF_FILENAME_PERMANENT = "AppVersionChecker_PREF_DATA_PERM";

	// version
	public static final String VERSION = "AppVersionChecker_Version";
	public static final String VERSION_DATETIME = "AppVersionChecker_VersionDateTime";
	public static final String VERSION_RELEASENOTES = "AppVersionChecker_VersionReleaseNotes";
    public static final String VERSION_USERIGNOREDVERSION = "AppVersionChecker_VersionUserIgnored";
}