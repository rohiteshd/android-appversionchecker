package ctnxa.appversionchecker.Utils;

public class StringUtils {
    // replacements
    static String singleQuote = "'";
    static String singleQuoteReplacementSQL = "\\\'";
    static String doubleQuote = "\"";
    static String doubleQuoteReplacementSQL = "\\\"";
    
    // decode a strng for SQL queries
    public static String decodeSQL(String str){
    	int startPoint = 0;
    	StringBuffer newStr;
    	
    	// proceed if string is NOT null
    	if(str != null){
        	newStr = new StringBuffer(str);
        	
        	// replace single quote replacements
        	while((startPoint = newStr.indexOf(singleQuoteReplacementSQL, startPoint))>=0){
        		newStr.replace(startPoint, startPoint+singleQuoteReplacementSQL.length(), singleQuote);
        		startPoint += singleQuote.length();
        	}
        	
        	// replace double quote replacements
        	startPoint = 0;
        	while((startPoint = newStr.indexOf(doubleQuoteReplacementSQL, startPoint))>=0){
        		newStr.replace(startPoint, startPoint+doubleQuoteReplacementSQL.length(), doubleQuote);
        		startPoint += doubleQuote.length();
        	}
        	
        	// set string as the new stirng
        	str = newStr.toString();
    	}
    	
    	return str;
    }

    // encode a string for SQL queries
    public static String encodeSQL(String str) {
        int startPoint = 0;
        StringBuffer newStr;
        
        // proceed if string is NOT null
        if(str != null){
        	newStr = new StringBuffer(str);
        	
            // replace for single quotes
            while ((startPoint = newStr.indexOf(singleQuote, startPoint)) >= 0) {
            	// if escape character already exists
            	if(startPoint>0 && newStr.charAt(startPoint-1)=='\\'){
            		startPoint++;
            		continue;
            	}
            		
                newStr.replace(startPoint, startPoint + singleQuote.length(), singleQuoteReplacementSQL);
                startPoint += singleQuoteReplacementSQL.length();
            }

            // replace for double quotes
            startPoint = 0;

            while ((startPoint = newStr.indexOf(doubleQuote, startPoint)) >= 0) {
                newStr.replace(startPoint, startPoint + doubleQuote.length(), doubleQuoteReplacementSQL);
                startPoint += doubleQuoteReplacementSQL.length();
            }
            
            // replace the old string with the new string
            str = newStr.toString();
        }

        return str;
    }
    
    // check if string is valid
    public static boolean isValid(String str){
    	boolean isValid = str!=null && str.length()>0;
    	
    	return isValid;
    }
}