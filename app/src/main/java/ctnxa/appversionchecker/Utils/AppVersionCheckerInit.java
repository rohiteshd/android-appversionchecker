package ctnxa.appversionchecker.Utils;

import android.content.Context;

public class AppVersionCheckerInit {
    public static final String appVersion_broadcastIntentName = "ctnxa.appVersionChange.broadcastIntentName";

    boolean isLog;
    boolean isShowMessage;
    int serverRefreshRate;
    int smallVersionChange_threshold;
    String packageName;
    Context context;

    // constructor
    public AppVersionCheckerInit(boolean isLog, boolean isShowMessage, String packageName, Context context){
        this.isLog = isLog;
        this.isShowMessage = isShowMessage;
        this.smallVersionChange_threshold = -1;
        this.packageName = packageName;
        this.context = context;

        getAppVersion();
    }

    // more parameters constructor
    public AppVersionCheckerInit(boolean isLog, boolean isShowMessage, int smallVersionChange_threshold, String packageName, Context context){
        this.isLog = isLog;
        this.isShowMessage = isShowMessage;
        this.serverRefreshRate = serverRefreshRate;
        this.smallVersionChange_threshold = smallVersionChange_threshold;
        this.packageName = packageName;
        this.context = context;

        getAppVersion();
    }

    // get the app version
    private void getAppVersion(){
        new Thread(new MainThread(isLog, isShowMessage, smallVersionChange_threshold, packageName, context)).start();

        // log
        HelperUtils.log(isLog, "AppVersionChecker Initialized!");
    }
}