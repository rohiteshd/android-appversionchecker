package ctnxa.appversionchecker.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.Calendar;
import java.util.HashMap;

import ctnxa.appversionchecker.Beans.AppVersionChange;
import ctnxa.appversionchecker.R;

public class MainThread extends Thread {
    // server refresh rate (hours)
    public static final int SERVERREFRESHRATE = 24;

    boolean isLog;
    boolean isShowMessage;
    int smallVersionChange_threshold;
    String packageName;
    Context context;

    // more parameters constructor
    public MainThread(boolean isLog, boolean isShowMessage, int smallVersionChange_threshold, String packageName, Context context) {
        this.isLog = isLog;
        this.isShowMessage = isShowMessage;
        this.smallVersionChange_threshold = smallVersionChange_threshold;
        this.packageName = packageName;
        this.context = context;
    }

    // is call server
    private boolean isServerCall(){
        String version = HelperUtils.getSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION, context);
        String serverCallDate = HelperUtils.getSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION_DATETIME, context);
        Calendar serverCall_cal, now_cal = TimeUtils.getCalendar(true);
        boolean isServerCall = !StringUtils.isValid(version);

        // check if version exists and if it does, has been refreshed
        if(!isServerCall){
            serverCall_cal = TimeUtils.getCalendar(serverCallDate, Calendar.HOUR, SERVERREFRESHRATE, true);
            isServerCall = serverCall_cal.before(now_cal);
        }

        return isServerCall;
    }

    // check app version
    private void checkAppVersion(){
        // call server if necessary else get value from cache
        if(isServerCall())
            getAppVersionFromServer();
        else
            broadcastAppVersion();
    }

    // show app upgrade message
    private void showMessage(final AppVersionChange appVersionChange, final String packageName){
        String alertMessage = "A newer version of the app is now available. ";
        AlertDialog.Builder alertDialogBuilder;
        boolean isUserIgnored;

        // app version change
        if(context!=null && appVersionChange!=null && (appVersionChange.isBigVersionChange() || appVersionChange.isSmallVersionChange())){
            // set alert message
            alertMessage += appVersionChange.isBigVersionChange() ?
                    "You NEED to upgrade to proceed!" :
                    "Would you like to upgrade from "+appVersionChange.getClientVersion()+" to "+appVersionChange.getServerVersion()+"?";

            // create the alert dialog
            alertDialogBuilder = new AlertDialog.Builder(context, R.style.appVersionChecker_AlertDialogStyle)
                    .setTitle("Upgrade App")
                    .setCancelable(false)
                    .setMessage(alertMessage)
                    .setPositiveButton("Upgrade", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent newIntent = new Intent(Intent.ACTION_VIEW)
                                    .setData(Uri.parse(Urls.version_playstore + packageName))
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(newIntent);
                        }
                    });

            // cancel button ONLY if NOT a big version change
            if(!appVersionChange.isBigVersionChange())
                alertDialogBuilder.setNegativeButton("Later", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // set user ignored upgrade flag and cancel this dialog
                        HelperUtils.setSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION_USERIGNOREDVERSION, appVersionChange.getServerVersion(), context);
                        dialog.cancel();
                    }
                });

            // check if user has ignored
            isUserIgnored = HelperUtils.getSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION_USERIGNOREDVERSION, context)!=null &&
                    HelperUtils.getSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION_USERIGNOREDVERSION, context).
                            compareTo(appVersionChange.getServerVersion())==0;

            // show if big version change or user has not ignored
            if(appVersionChange.isBigVersionChange() || !isUserIgnored)
                alertDialogBuilder.create().show();
        }
    }

    // broadcast app version
    private void broadcastAppVersion(){
        String serverVersion, clientVersion, releaseNotes;
        final AppVersionChange appVersionChange;
        Intent intent;

        try {
            // instantiate app version change
            clientVersion = context.getPackageManager().getPackageInfo(packageName, 0).versionName;
            serverVersion = HelperUtils.getSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION, context);
            releaseNotes = HelperUtils.getSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION_RELEASENOTES, context);
            appVersionChange = new AppVersionChange(serverVersion, clientVersion, releaseNotes, smallVersionChange_threshold, isLog, context);

            // log
            HelperUtils.log(isLog, "PackageName : " + packageName + " ClientVersion : " + clientVersion + " ServerVersion : " + serverVersion);

            // send broadcast
            intent = new Intent(AppVersionCheckerInit.appVersion_broadcastIntentName);
            intent.putExtra("appVersionChange", appVersionChange);
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);

            // show message by running this on a UI thread
            if(isShowMessage && context!=null && (context instanceof Activity))
                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showMessage(appVersionChange, packageName);
                    }
                });
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    // get the app version from server
    public void getAppVersionFromServer(){
        String version = null, releaseNotes = null;
        String url = Urls.version_playstore + packageName;
        String response = new NetworkUtils(context, null, true, url).getResponse();
        boolean isVersionValid = false;
        Parser parser = new Parser(context);
        HashMap<String, String> extraParams;

        // get version and release notes if valid response
        if(StringUtils.isValid(response)){
            // release notes
            releaseNotes = parser.parseValueFromHTML(response, "releaseNotes", null);

            // version
            extraParams = new HashMap<>(); extraParams.put("releaseNotes", releaseNotes);
            version = parser.parseValueFromHTML(response, "version", extraParams);

            // check version is in the format <integer>.<integer>.<integer>
	        isVersionValid = StringUtils.isValid(version);
            for(int x=0; version!=null && isVersionValid && x<version.split("\\.").length; x++)
                try {
                    // check part is integer
                    Integer.parseInt(version.split("\\.")[x]);
                } catch(NumberFormatException e){
                    // only exception is -debug
                    isVersionValid = version.split("\\.")[x].contains("-debug") ? true : false;
                }
        }

        // log
        HelperUtils.log(isLog, "App Version From Playstore : " + version);
        HelperUtils.log(isLog, "Release Notes From Playstore : " + releaseNotes);

        // valid server response for app version
        if(StringUtils.isValid(version)){
            // store version in shared preference
            HelperUtils.setSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION, version, context);
            HelperUtils.setSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION_RELEASENOTES, releaseNotes, context);
            HelperUtils.setSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION_DATETIME, TimeUtils.getTimestamp(null, false, true), context);

            // send app version
            broadcastAppVersion();
        }
    }

    @Override
    public void run() {
        checkAppVersion();
    }
}