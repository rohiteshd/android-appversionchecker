package ctnxa.appversionchecker.Beans;

import android.content.Context;

import java.io.Serializable;

import ctnxa.appversionchecker.Utils.Constants;
import ctnxa.appversionchecker.Utils.HelperUtils;
import ctnxa.appversionchecker.Utils.StringUtils;

public class AppVersionChange implements Serializable {
    public static final int SMALLVERSIONCHANGE_THRESHOLD = 2;

    boolean isBigVersionChange;
    boolean isSmallVersionChange;
    int smallVersionChange_threshold;
    String serverVersion;
    String clientVersion;
    String releaseNotes;
    boolean isLog;
    Context context;

    // constructor
    public AppVersionChange(String serverVersion, String clientVersion, String releaseNotes, int smallVersionChange_threshold, boolean isLog, Context context){
        this.serverVersion = StringUtils.isValid(serverVersion) ? cleanupAppVersion(serverVersion) : null;
        this.clientVersion = cleanupAppVersion(clientVersion);
        this.releaseNotes = StringUtils.isValid(releaseNotes) ? releaseNotes : "";
        this.smallVersionChange_threshold = smallVersionChange_threshold>0 ? smallVersionChange_threshold : SMALLVERSIONCHANGE_THRESHOLD;
        this.isLog = isLog;
        this.context = context;
    }

    // clean up app version
    public static String cleanupAppVersion(String appVersion){
        String cleanupAppVersion = StringUtils.isValid(appVersion) && appVersion.contains("-") ? appVersion.substring(0, appVersion.indexOf("-")) : appVersion;

        return cleanupAppVersion;
    }

    // is server version newer than client version
    public boolean isNew(){
        boolean isNew = true;
        float serverVersion_float = 0, clientVersion_float = 0;

        // get float values
        try {
            serverVersion_float = Float.parseFloat(serverVersion);
            clientVersion_float = Float.parseFloat(clientVersion);
        } catch(NumberFormatException e){
            e.printStackTrace();
        }

        // check if server is newer than client
        isNew = serverVersion_float > clientVersion_float;

        return isNew;
    }

    // is there a change in the big version
    public boolean isBigVersionChange() {
        isBigVersionChange = false;

        // check proper versions are available
        if(StringUtils.isValid(serverVersion) && serverVersion.split("\\.").length>1 && StringUtils.isValid(clientVersion) && clientVersion.split("\\.").length>1)
            isBigVersionChange = Integer.parseInt(serverVersion.split("\\.")[0]) != Integer.parseInt(clientVersion.split("\\.")[0]) ||
                    Integer.parseInt(serverVersion.split("\\.")[1]) - Integer.parseInt(clientVersion.split("\\.")[1]) >= smallVersionChange_threshold;

        return isBigVersionChange;
    }

    // is there a change in the small version
    public boolean isSmallVersionChange() {
        isSmallVersionChange = false;

        // check proper versions are available
        if(StringUtils.isValid(serverVersion) && serverVersion.split("\\.").length>1 && StringUtils.isValid(clientVersion) && clientVersion.split("\\.").length>1)
            isSmallVersionChange = Integer.parseInt(serverVersion.split("\\.")[1]) != Integer.parseInt(clientVersion.split("\\.")[1]);

        return isSmallVersionChange;
    }

    public boolean isUserIgnored() {
        boolean isUserIgnored;
        String userIgnoredVersion = HelperUtils.getSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION_USERIGNOREDVERSION, context);

        // check if user ignored for this version
        isUserIgnored = userIgnoredVersion.compareTo(getServerVersion())==0;

        return isUserIgnored;
    }

    public String getServerVersion() {
        return serverVersion;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public String getReleaseNotes() {
        return releaseNotes;
    }
}
