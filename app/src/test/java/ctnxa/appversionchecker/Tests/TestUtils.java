package ctnxa.appversionchecker.Tests;

import android.content.Context;

import java.io.IOException;
import java.util.Properties;

public class TestUtils {
    static Properties props = null;

    // read props file
    public static String getProperty(String key, Context context){
        String val = null;

        // initialize props
        if(props == null)
            try {
                props = new Properties();
                props.load(ClassLoader.getSystemResourceAsStream("AppVersion.properties"));
            } catch (IOException e) {
                e.printStackTrace();
            }

        // get value
        val = props.getProperty(key);

        return val;
    }
}
