package ctnxa.appversionchecker.Tests;

import android.content.Context;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import ctnxa.appversionchecker.Utils.Constants;
import ctnxa.appversionchecker.Utils.HelperUtils;
import ctnxa.appversionchecker.Utils.MainThread;

@RunWith(RobolectricTestRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AppVersionTests {
    String expectedVersion, expectedReleaseNotes, packageName;
    Context context;

    @Before
    public void initialization(){
        // initialize values
        context  = RuntimeEnvironment.getApplication().getApplicationContext();
        expectedVersion = TestUtils.getProperty("test.version", context);
        expectedReleaseNotes = TestUtils.getProperty("test.releaseNotes", context);
        packageName = TestUtils.getProperty("test.package", context);
    }

    // test formatter is fetched from server (ALWAYS RUN FIRST)
    @Test
    public void isPlaystoreVersion(){
        String version, releaseNotes;

        // get app version from playstore
        new MainThread(false, false, 1, packageName, context).getAppVersionFromServer();

        // read version and release notes from the shared prefs
        version = HelperUtils.getSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION, context);
        releaseNotes = HelperUtils.getSharedPreferencesString(Constants.SHARED_PREF_FILENAME_PERMANENT, Constants.VERSION_RELEASENOTES, context);

        // verify correct app version and release notes
        Assert.assertNotNull(version); Assert.assertEquals(version, expectedVersion);
        Assert.assertNotNull(releaseNotes); Assert.assertEquals(releaseNotes, expectedReleaseNotes);
    }
}